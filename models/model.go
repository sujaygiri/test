package models

import "go.mongodb.org/mongo-driver/bson/primitive"

//"Userdata exporting"
type Userdata struct {
	ID        primitive.ObjectID `json:"_id,omitempty" bson:"_id,omitempty"`
	FirstName string             `json:"firstname" bson:"firstname" validate:"nonzero"`
	LastName  string             `json:"lastname" bson:"lastname"`
	Password  string             `json:"password" bson:"string"`
}
