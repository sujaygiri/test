package service

import (
	"context"
	"encoding/json"
	"fmt"
	"models"
	"net/http"
	"strconv"

	"helper"
	"log"
	"logger"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo/options"

	"gopkg.in/validator.v2"
)

func Users(response http.ResponseWriter, req *http.Request) {
	response.Header().Set("Content-Type", "application/json")
	var incominguser models.Userdata
	decoder := json.NewDecoder(req.Body)
	decoder.Decode(&incominguser)
	err := validator.Validate(incominguser)
	if err != nil {
		sugarlogger := logger.ReturnLogger()
		sugarlogger.Errorf("first name is blank in the request")
		sugarlogger.Infof("first name is blank")

		response.WriteHeader(http.StatusBadRequest)
		body := make(map[string]string)
		body["error"] = "bad body ,first name shouldnt be blank"
		json.NewEncoder(response).Encode(body)

	} else {

		mongoconnection := helper.ConnectDB()
		result, err := mongoconnection.InsertOne(context.TODO(), incominguser)
		if err != nil {
			log.Fatal(err)
			fmt.Println("check database")
		}
		response.WriteHeader(http.StatusCreated)
		json.NewEncoder(response).Encode(result)
	}
}

func Getusers(response http.ResponseWriter, req *http.Request) {
	response.Header().Set("Content-Type", "application/json")
	var users []models.Userdata
	mongocollection := helper.ConnectDB()
	letoptions := options.Find()
	var page int64
	var limit int64
	page, err := strconv.ParseInt(req.URL.Query().Get("page"), 10, 64)
	limit, err = strconv.ParseInt(req.URL.Query().Get("pagesize"), 10, 64)
	letoptions.SetLimit(limit)
	letoptions.SetSkip((page - 1) * limit)
	cur, err := mongocollection.Find(context.TODO(), bson.M{}, letoptions)
	if err != nil {
		fmt.Println("database error")

	}
	defer cur.Close(context.TODO())
	for cur.Next(context.TODO()) {
		var singleuser models.Userdata
		err := cur.Decode(&singleuser)
		if err != nil {
			fmt.Println("fetching error")
			log.Fatal(err)
		}

		users = append(users, singleuser)
	}

	if err := cur.Err(); err != nil {
		log.Fatal(err)
	}
	json.NewEncoder(response).Encode(users)
}

func Getuser(response http.ResponseWriter, req *http.Request) {
	var singleuser models.Userdata
	response.Header().Set("Content-type", "application/json")
	mongocollection := helper.ConnectDB()
	id, _ := primitive.ObjectIDFromHex(req.URL.Query().Get("id"))
	filter := bson.M{"_id": id}
	err := mongocollection.FindOne(context.TODO(), filter).Decode(&singleuser)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		body := make(map[string]string)
		body["error"] = "no data with id found"
		json.NewEncoder(response).Encode(body)
	} else {
		json.NewEncoder(response).Encode(singleuser)
	}
}
func Deleteuser(response http.ResponseWriter, req *http.Request) {
	response.Header().Set("content-type", "application/json")
	id, _ := primitive.ObjectIDFromHex(req.URL.Query().Get("id"))
	filter := bson.M{"_id": id}
	mongocollection := helper.ConnectDB()
	result, err := mongocollection.DeleteOne(context.TODO(), filter)
	fmt.Println(err)
	if err != nil {
		response.WriteHeader(http.StatusBadRequest)
		body := make(map[string]string)
		body["error"] = "object with object id doesnot exist"
		json.NewEncoder(response).Encode(body)
	} else {
		response.WriteHeader(http.StatusNoContent)
		json.NewEncoder(response).Encode(result)
	}

}
