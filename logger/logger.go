package logger

import "go.uber.org/zap"

var sugar *zap.SugaredLogger

func init() {
	logger, err := zap.NewProduction()
	if err != nil {
		panic("Cannot initialize zap logger")
	}
	sugar = logger.Sugar()
}

func ReturnLogger() *zap.SugaredLogger {
	return sugar
}
